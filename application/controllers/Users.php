<?php
class Users extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('User_model');
         $this->load->library('form_validation'); 
    }
    function index(){
         $getcategorydata=$this->User_model->getdata("category");
         $data['getcategorydata']=$getcategorydata;

        $this->load->view('user_view',$data);
    }
//this function used for get list of user record
    function getuserlist(){
        $getuserdata=$this->User_model->getdata("users");
        $getcategorydata=$this->User_model->getdata("category");
        $html='';
        $i=1;
        $data='';
        if($getuserdata){
        foreach($getuserdata as $data){
            $conds=array('c_ID'=>$data->category);
            $getcatedata=$this->User_model->fetchrow("category",$conds);

                    $game_chk="";
                    $Programming_chk="";
                    $Reading_chk="";
                    $Photography_chk="";

                    $hobby_arr=explode(',', $data->hobby);
                     if(in_array('Games', $hobby_arr)){
                        $game_chk="checked";
                     }
                     if(in_array('Programming', $hobby_arr)){
                        $Programming_chk="checked";
                     }
                     if(in_array('Reading', $hobby_arr)){
                        $Reading_chk="checked";
                     }
                     if(in_array('Photography', $hobby_arr)){
                        $Photography_chk="checked";
                     }

                    $selecthtml="<select name='category'><option value=''>Select category</option>";
                    foreach ($getcategorydata as $keyvalue) { 
                    $select='';
                    if($keyvalue->c_ID==$data->category){
                    $select="selected";
                    }
                    $selecthtml.='<option value="'.$keyvalue->c_ID.'" '.$select.'>'.$keyvalue->name.'</option>';
                    } 
                    $selecthtml.='</select>';

            $html .= '<tr class="edit_tr_'.$data->id.'" id="trdata">'.
                    '<td>'.$i.'</td>
                    <td><input type="checkbox"  name="bulk_delete[]" value="'.$data->id.'" class="checkbox"></td>
                    <td><span class="input_hide"><input type="text" class="editInput" name="first_name" value="'.$data->first_name.'"></span><span class="data_show">'.$data->first_name.'</span></td>
                    <td><span class="input_hide"><input type="text" class="editInput" name="contact_no" value="'.$data->contact_no.'"></span><span class="data_show">'.$data->contact_no.'</span></td>
                    <td><span class="input_hide chksec"><input type="checkbox"  name="hobby[]" value="Programming" class="editInputhb" '.$Programming_chk.'>Programming<br>
                    <input type="checkbox" name="hobby[]" class="editInputhb" value="Games" '.$game_chk.'>Games<br>
                    <input type="checkbox" name="hobby[]" class="editInputhb" value="Reading" '.$Reading_chk.'>Reading<br>
                    <input type="checkbox" name="hobby[]" class="editInputhb" value="Photography" '.$Photography_chk.'>Photography<br></span><span class="data_show">'.$data->hobby.'</span></td>
                    <td><span class="input_hide">'.$selecthtml.'</span><span class="data_show">'.$getcatedata->name.'</span></td>
                   <td><span class="input_hide"><input type="hidden" class="editInput" name="hidimg" value="'.$data->profile_pic.'"><input type="file" name="image" id="img_'.$data->id.'"></span><img src='.base_url('assets/upload/'.$data->profile_pic).' width="100px" hight="100px"></td>
                   <td style="text-align:right;">'.
                    '<a href="javascript:void(0);" class="btn btn-info btn-sm item_edit" data-first_name="'.$data->first_name.'" data-contact_no="'.$data->contact_no.'"  data-category="'.$data->category.'"   data-hobby="'.$data->hobby.'"  data-image="'.$data->profile_pic.'"  data-id="'.$data->id.'">Edit</a><a href="javascript:void(0);" class="btn btn-info btn-sm saveBtn" data-id="'.$data->id.'" style="display:none">Save</a> '.' '.
                        '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_delete" data-deleteid="'.$data->id.'">Delete</a>'.
                    '</td>'.
                    '</tr>';
                    $i++;
                    }
                    $data=$html;
             }else{
                $data='';
             }
        echo json_encode($data);
    }
//this fnction used for save user details
    function save(){
        
            $this->form_validation->set_rules('first_name', 'First Name', 'required'); 
            $this->form_validation->set_rules('category', 'Category', 'required'); 
            $this->form_validation->set_rules('contact_no', 'Contact Number', 'required');
            $this->form_validation->set_rules('hobby[]', 'Hobby', 'required');
          
            if($this->form_validation->run() == true){ 
                /*for image uploade*/
                $uploadPath = 'assets/upload/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png';
                
                // Load and initialize upload library
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                // Upload file to server
                if($this->upload->do_upload('image')){
                    // Uploaded file data
                    $fileData = $this->upload->data();
                    $uploadData['file_name'] = $fileData['file_name'];
                    $file_name=$fileData['file_name'];
                    $file_name=str_replace(' ', '-',$uploadData['file_name']); 

                }
                /*end image uploade*/
                $hobby=implode(',', $this->input->post('hobby'));
                $postData = array( 
                'first_name' => strip_tags($this->input->post('first_name')), 
                'contact_no' => strip_tags($this->input->post('contact_no')), 
                'category' => strip_tags($this->input->post('category')), 
                'hobby' => strip_tags($hobby),
                'profile_pic' => strip_tags($file_name) 
            ); 

                $insert = $this->User_model->insert('users',$postData); 
                if($insert){ 
                   
                     $data['success'] = 'Your data save successfully'; 
                }else{ 
                    $data['error_msg'] = 'Some problems occured, please try again.'; 
                } 
            }else{ 
                $data['error_msg'] = validation_errors(); 
            } 
        

        echo json_encode($data);
    }
//this fnction used for update serr data
    function update(){
        error_reporting(0);
            if($this->input->post()){ 
                if($_FILES['file']!=''){
                /*for image uploade*/
                $uploadPath = 'assets/upload/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'jpg|jpeg|png';
                
                // Load and initialize upload library
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                
                // Upload file to server
                if($this->upload->do_upload('file')){
                    // Uploaded file data
                    $fileData = $this->upload->data();
                    $uploadData['file_name'] = $fileData['file_name'];
                    $file_name=$fileData['file_name'];
                    $file_name=str_replace(' ', '-',$uploadData['file_name']); 

                }
                /*end image uploade*/
                }else{
                    $file_name=strip_tags($this->input->post('hideimage'));
                }
                
                $postData = array( 
                'first_name' => strip_tags($this->input->post('first_name')), 
                'contact_no' => strip_tags($this->input->post('contact_no')), 
                'category' => strip_tags($this->input->post('category')), 
                'hobby' => strip_tags($this->input->post('hobby')),
                'profile_pic' => strip_tags($file_name) 
            ); 
                $cond= array('id'=>$this->input->post('id'));
                $update = $this->User_model->updatesingleconddata('users',$postData,$cond); 
                if($update){ 
                   
                     $data['success'] = 'Your account data has been updated successfully'; 
                }else{ 
                    $data['error_msg'] = 'Some problems occured, please try again.'; 
                } 
            }else{ 
                $data['error_msg'] = validation_errors(); 
            } 
    
        return true;
    }
// this function used for delete user single record
    function delete(){
         $cond= array('id'=>$this->input->post('id'));
         
         $delete = $this->User_model->delete_data('users',$cond); 
                if($delete){ 
                    $data['success'] = 'Your  data has been deleted successfully';
                }else{
                    $data['error_msg'] = 'Your  data has been deleted successfully';
                }
        echo json_encode($data);
    }
//this fnction used for delete multiple user recorrd
    function bulkdelete(){
        
        $delete = $this->User_model->delete_selected_data('users','id',$this->input->post('bulk_record')); 
                if($delete){ 
                    $data['success'] = 'Your  data has been deleted successfully';
                   
                }else{
                     $data['error_msg'] = 'Your  data has been deleted successfully';
                    
                }
        echo json_encode($data);
    }

}