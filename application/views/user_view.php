<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>User List</title>
<script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap.css'?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/jquery.dataTables.css'?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/dataTables.bootstrap4.css'?>">

<style type="text/css">
.error{
color:red;
}

</style>
</head>
<body>
<div class="container">
              	<!-- Page Heading -->
                  <div class="row">
                      <div class="col-12">
                          <div class="col-md-12">
                              <h1>Users
                                  <small>List</small>
                                  <div class="float-right"><a href="javascript:void(0);" class="btn btn-primary addnewdata"><span class="fa fa-plus"></span> Add New</a><a href="javascript:void(0);" class="btn btn-primary bulkdeletedata"><span class="fa fa-plus"></span> Bulk Delete</a></div>
                              </h1>
                          </div>
                          <?php echo $this->session->flashdata('message_name');?>
                          <table class="table table-striped" id="mydata">
                              <thead>
                                  <tr>
                                          <th class="no-sorts">Srno.</th>
                                          <th class="no-sorts">Select <input type="checkbox" name="allchk" class="select_all"></th>
                                          <th class="no-sorts">Name</th>
                                          <th class="no-sorts">Conatct No</th>
                                          <th class="">Hobby</th>
                                          <th class="ss">Category</th>
                                          <th class="no-sorts" width="100px;">Profile pic</th>
                                          <th style="text-align: right;">Actions</th>
                                  </tr>
                              </thead>
                              <tbody id="show_data">
                                  
                              </tbody>
                          </table>
                      </div>
                  </div>
                      
              </div>
		        <!-- MODAL ADD -->
              <form class="form-horizontal" role="form" method="post" id="register"  enctype="multipart/form-data">
            <div class="modal fade" id="Modal_Add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">  
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <span style="color:red;text-align:center;margin:100px 235px" class="all_error"></span>
                    <span style="color:green;text-align:center;margin:100px 235px" class="success"></span>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Name</label>
                            <div class="col-md-10">
                             <input type="text" name="first_name" placeholder="NAME" value=""  class="form-control" value="<?php echo !empty($user->first_name)?$user->first_name:''; ?>">
                             <?php echo form_error('first_name','<p class="help-block">','</p>'); ?>
                            </div>
                        </div>
                          <div class="form-group row">
                            <label class="col-md-2 col-form-label">Contact No</label>
                            <div class="col-md-10">
                             <input type="text" id="contact_no" name="contact_no" placeholder="Contact number" class="form-control" value="<?php echo !empty($user->contact_no)?$user->contact_no:''; ?>">
                            <?php echo form_error('contact_no','<p class="help-block">','</p>'); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Hobby</label>
                            <div class="col-md-10 ">
                                <div class="hberror">
                              <input type="checkbox" name="hobby[]" value="Programming">Programming
                              <input type="checkbox" name="hobby[]" value="Games">Games
                              <input type="checkbox" name="hobby[]" value="Reading">Reading
                              <input type="checkbox" name="hobby[]" value="Photography">Photography
                          </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-2 col-form-label">Category</label>
                            <div class="col-md-10">
                              <select name="category" class="form-control">
                                <option value="">Select Category</option>
                                <?php foreach ($getcategorydata as $keyvalue) { ?>
                                <option value="<?php echo $keyvalue->c_ID;?>"><?php echo $keyvalue->name;?></option>
                                <?php } ?>
                              </select>
                             
                                <?php echo form_error('category','<p class="help-block">','</p>'); ?>
                            </div>
                        </div>
                         <div class="form-group row">
                            <label class="col-md-2 col-form-label">Profile Image</label>
                            <div class="col-md-10">
                              <input type="file" id="image" name="image" placeholder="image" class="form-control imgfile">
                                <?php echo form_error('image','<p class="help-block">','</p>'); ?>
                            </div>
                        </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <input type="submit" name="sss" value="Save" id="btn_save" class="btn btn-primary">
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL ADD-->
       <!--MODAL DELETE-->
         <form >
            <div class="modal fade" id="Modal_Delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Record</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                       <strong>Are you sure to delete this record?</strong>
                  </div>
                  <div class="modal-footer">
                    <input type="hidden" name="data_delete" id="data_delete" class="form-control">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" type="submit" id="btn_delete" class="btn btn-primary">Yes</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL DELETE-->
         <!--MODAL bulk DELETE-->
         <form >
            <div class="modal fade" id="Modal_Delete_bulk" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete bulk Record</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                       <strong>Are you sure to delete bulk record?</strong>
                  </div>
                  <div class="modal-footer">
                    <input type="hidden" name="data_delete_bulk" id="data_delete_bulk" class="form-control">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="button" type="submit" id="btn_delete_bulk" class="btn btn-primary">Yes</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL DELETE-->
       <!--MODAL bulk DELETE-->
         <form >
            <div class="modal fade" id="Modal_Delete_bulk_error" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete bulk Record</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                       <strong style="color: red">Please tick checkbox record you want to delete from list</strong>
                  </div>
                  <div class="modal-footer">
                   
                  </div>
                </div>
              </div>
            </div>
            </form>
        <!--END MODAL DELETE-->
<script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.dataTables.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/dataTables.bootstrap4.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.validate.min.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url().'assets/js/additional-methods.min.js';?>"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.addnewdata').on('click',function(){
          $("label.error").css('display','none');
          $("#register").trigger("reset");
          $("#registeredt").trigger("reset");
          $('#Modal_Add').modal('show');
        });
        $('.all_error').html('');
        $('.all_erroredt').html('');
       $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z]*$/);
    });
    $('#register').validate({
    ignore: [],
      rules: {
          first_name:{
              required:true,
              lettersonly:true,
              maxlength:10,           
          },
          contact_no:{
              required:true,
              number:true 
          },
          category:"required",
          image:{
              required:true,
              extension:"jpg|jpeg|png" 
          },
         "hobby[]":"required",
       },
      messages: {
          first_name:{required:"Please enter  name.", lettersonly:"Please enter valid  name."},
          contact_no:{
                    required:"Please enter contact number.",
                    number:"Please enter valid contact."
                     },
          category:{
                    required:"Please select category."
                   },
          image:{
                  required:"please uploade image",
                  extension:"please uploade only jpg,jpeg and png image." 
                },
          "hobby[]":"Please select hobby",
      }, 
      errorPlacement: function(error, element) {
          if(element.attr('name') == "hobby[]"){
              jQuery('.hberror').after(error);
          }
          else if(element.attr('name') == "image"){
              jQuery('.imgfile').after(error);//.append(error);
          }else{
              error.insertAfter(element);
          }
      },     
      submitHandler: function(form) {
              $('.all_error').html('');
              $.ajax({
                  type : "POST",
                  url  : "<?php echo site_url('Users/save')?>",
                  data: new FormData(form),
                  mimeType: "multipart/form-data",
                  contentType: false,
                  cache: false,
                  processData: false,
                  success: function(data){
                      var response=JSON.parse(data);
                      console.log(response);
                      if(response.error_msg!=''){
                          $('.all_error').html(response.error_msg);
                      }
                      if(response.error_msg!=''){
                          $('.success').html(response.error_msg);
                      }else{

                      }
                     $('#Modal_Add').modal('hide');
                      show_users();
                  }
              });
              return false;
      }
});
  
//call fnction for get all user
show_users();   

   $("#mydata").dataTable({
                    "lengthChange": false,
                    "bPaginate": true, 
                    "searching": false,
                    "bJQueryUI": false,
                    "ordering": false,
                    "info": false,
                    "language": {
                          "emptyTable": "No Data Found."
                      },
                    
                  }); 

                  
// ticked all checkox for delete
$(document).on("change",".select_all",function() {
  if($(".select_all").prop('checked') == true){
$(".checkbox").prop("checked", true);
  }else{
     $(".checkbox").removeAttr("checked");
  }
});    
      // list user data
        function show_users(){
            $.ajax({
                type  : 'ajax',
                url   : '<?php echo site_url('Users/getuserlist')?>',
                async : false,
                dataType : 'json',
                success : function(data){
                   
                    $('#show_data').html(data);
                    $('span.input_hide').css('display','none');
                }

            });
        }


        //get data for update record
        $('#show_data').on('click','.item_edit',function(){
          var id=$(this).attr('data-id');
           $(this).closest("tr").find(".data_show").hide();
           $(this).closest("tr").find(".input_hide").show();
           $(this).css('display','none');
           $(this).closest("tr").find(".saveBtn").show();
          
        });
        // saved edit inline data
        $(document).on("click",".saveBtn",function() {
        $(this).closest("tr").find('.error').remove();
        var trObj = $(this).closest("tr");
        var ID = $(this).attr('data-id');
       
        var first_name = $(this).closest("tr").find("input[name='first_name']").val();
        var contact_no = $(this).closest("tr").find("input[name='contact_no']").val();
        var category = $(this).closest("tr").find("select[name='category']").val();
        var hideimage = $(this).closest("tr").find("input[name='hidimg']").val();

        var checkedVals = $(this).closest("tr").find('.editInputhb:checkbox:checked').map(function() {
        return this.value;
        }).get();
        checkedVals.join(",")
        var file_data = $("#img_"+ID).prop("files")[0];   
        var form_data = new FormData();

        /*validation*/
        if(first_name==''){
          $(this).closest("tr").find("input[name='first_name']").after('<span class="error">Please enter name.</span>');
        }
        if(contact_no==''){
          $(this).closest("tr").find("input[name='contact_no']").after('<span class="error">Please enter contact number.</span>');
        } 
        if(category==''){
          $(this).closest("tr").find("select[name='category']").after('<span class="error">Please select category.</span>');
        }
        if(checkedVals==''){
          $(this).closest("tr").find('.chksec').after('<span class="error">Please select hobby.</span>');
        }      
        if(first_name=='' || contact_no=='' || category=='' || checkedVals==''){
           return false;
        }
                  
        form_data.append("file", file_data)            
        form_data.append("id", ID)
        form_data.append("hobby", checkedVals)
        form_data.append("first_name", first_name)
        form_data.append("contact_no", contact_no)
        form_data.append("category", category)
        form_data.append("hideimage", hideimage)
        
        $.ajax({
                type : "POST",
                url  : "<?php echo site_url('Users/update')?>",
                dataType : "script",
                 cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                success : function(data){
                    show_users();
                }
            })
            return false;
  });
        //get data for delete record
        $('#show_data').on('click','.item_delete',function(){
            var deleteid = $(this).data('deleteid');
            
            $('#Modal_Delete').modal('show');
            $('[name="data_delete"]').val(deleteid);
        });

         //get data for bulk delete record
        $('.bulkdeletedata').on('click',function(){
           var bulk_record = [];
            $("input:checkbox[name='bulk_delete[]']:checked").each(function(){
              bulk_record.push($(this).val());
            });
            if(bulk_record.length>=1){
               $('#Modal_Delete_bulk').modal('show');
             }else{

          $('#Modal_Delete_bulk_error').modal('show');
             }
           
            
        });

        //delete single record to database
         $('#btn_delete').on('click',function(){
            var id = $('#data_delete').val();
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('Users/delete')?>",
                dataType : "JSON",
                data : {id:id},
                success: function(data){
                    $('[name="data_delete"]').val("");
                    $('#Modal_Delete').modal('hide');
                    show_users();
                }
            });
            return false;
        });

         //delete bulk record to database
         $('#btn_delete_bulk').on('click',function(){
            var id = $('#data_delete').val();
            var bulk_record = [];
            $("input:checkbox[name='bulk_delete[]']:checked").each(function(){
              bulk_record.push($(this).val());
            });
            
            $.ajax({
                type : "POST",
                url  : "<?php echo site_url('Users/bulkdelete')?>",
                dataType : "JSON",
                data : {bulk_record:bulk_record},
                success: function(data){
                   show_users();
                    $('#Modal_Delete_bulk').modal('hide');
                   
                }
            });
            return false;
        });

});
</script>
</body>
</html>